package app.auth.service;

import app.auth.domain.User;

/**
 * Created by almagnit on 11.09.2016.
 */
public class UserFingerprint {

    private Long id;

    private String password;

    private Long expires;

    public UserFingerprint(User user) {
        this.id = user.getId();
        this.password = user.getPassword();
        this.expires = user.getExpires();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getExpires() {
        return expires;
    }

    public void setExpires(Long expires) {
        this.expires = expires;
    }
}
