package app.auth.configuration;

import app.auth.service.UserSessionService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by almagnit on 11.09.2016.
 */

class StatelessAuthenticationFilter extends GenericFilterBean {
    private final UserSessionService userSessionService;

    protected StatelessAuthenticationFilter(UserSessionService taService) {
        this.userSessionService = taService;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
            ServletException {
        SecurityContextHolder.getContext().setAuthentication(
                userSessionService.getAuthentication((HttpServletRequest) req));
        chain.doFilter(req, res); // always continue
    }
}
