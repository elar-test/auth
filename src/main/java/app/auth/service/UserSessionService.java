package app.auth.service;

import app.auth.domain.User;
import app.auth.domain.UserSession;
import app.auth.repository.UserSessionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;

/**
 * Created by almagnit on 11.09.2016.
 */

@Service
public class UserSessionService {

    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";
    private static final long TEN_DAYS = 1000 * 60 * 60 * 24 * 10;

    private final TokenHandler tokenHandler;

    private String token;

    @Autowired
    UserSessionRepository userSessionRepository;

    @Autowired
    public UserSessionService(@Value("${app.token.secret}") String secret,
                              ObjectMapper objectMapper) {
        tokenHandler = new TokenHandler(DatatypeConverter.parseBase64Binary(secret), objectMapper);
    }

    public void addAuthentication(HttpServletResponse response, UserAuthentication authentication) {
        final User user = authentication.getDetails();
        user.setExpires(System.currentTimeMillis() + TEN_DAYS);
        UserSession session = new UserSession();
        session.setUser(user);
        session.setExpires(user.getExpires());
        session.setToken(tokenHandler.createTokenForUser(user));
        userSessionRepository.save(session);
        response.addHeader(AUTH_HEADER_NAME, session.getToken());
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        token = request.getHeader(AUTH_HEADER_NAME);
        if(token == null){
            Arrays.asList(request.getCookies()).forEach(item -> {
                if(item.getName().equals(AUTH_HEADER_NAME)){
                    try {
                        token = URLDecoder.decode(item.getValue(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        if (token != null) {
            UserSession userSession = userSessionRepository.findOneByToken(token.trim());
            if (userSession != null) {
                final User user = userSession.getUser();
                if (user.getEnabled()) {
                    user.setExpires(userSession.getExpires());
                    return new UserAuthentication(user);
                }
            }
        }
        return null;
    }

    public String createTokenAndSession(User user) {
        user.setExpires(System.currentTimeMillis() + TEN_DAYS);
        UserSession session = new UserSession();
        session.setUser(user);
        session.setExpires(user.getExpires());
        session.setToken(tokenHandler.createTokenForUser(user));
        userSessionRepository.save(session);

        return session.getToken();
    }

    public String generateTokenForUser(User user) {
        return tokenHandler.createTokenForUser(user);
    }

}
