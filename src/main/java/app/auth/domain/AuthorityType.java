package app.auth.domain;

/**
 * Created by almagnit on 11.09.2016.
 */
public enum AuthorityType {

    ROLE_ADMIN,
    ROLE_USER

}
