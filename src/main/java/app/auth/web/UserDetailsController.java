package app.auth.web;

import app.auth.domain.User;
import app.auth.service.CurrentUserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by almagnit on 11.09.2016.
 */

@RestController
public class UserDetailsController {

    @Autowired
    CurrentUserSecurityService currentUserSecurityService;

    @ResponseStatus
    @RequestMapping(value = "/api/details", method = RequestMethod.GET)
    public String index(){
        User user = currentUserSecurityService.getUser();
        String result = "Пользователь не найден";
        if(user != null){
            result = user.getUsername() + ", добро пожаловать!";
        }
        return result;
    }

}
