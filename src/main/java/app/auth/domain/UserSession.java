package app.auth.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by almagnit on 11.09.2016.
 */

@Entity
@Table(name = "user_sessions")
@Data
@EqualsAndHashCode(callSuper = true)
public class UserSession extends AppAbstractPersistable {

    protected long expires;

    private String token;

    @ManyToOne
    private User user;

}
