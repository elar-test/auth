package app.auth.web;

import app.auth.domain.Authority;
import app.auth.domain.AuthorityType;
import app.auth.domain.User;
import app.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;

/**
 * @author Aleksey
 * @since 06.10.2016 10:00
 */

@RestController
public class SignupController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public void signup(@RequestBody User user){
        user.setAuthorities(new HashSet<>());
        user.addAuthority(new Authority(AuthorityType.ROLE_USER));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEmail(user.getUsername());
        userRepository.save(user);
    }

}
