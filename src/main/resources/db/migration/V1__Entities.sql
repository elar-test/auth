CREATE EXTENSION IF NOT EXISTS "pgcrypto" WITH SCHEMA public;

CREATE TABLE users (
  id                     BIGSERIAL PRIMARY KEY,
  enabled                 BOOLEAN      NOT NULL DEFAULT FALSE,
  email                   VARCHAR(250) NOT NULL,
  username                VARCHAR(250) NOT NULL,
  password                VARCHAR(250) NOT NULL
);

CREATE TABLE authorities (
  id                     BIGSERIAL PRIMARY KEY,
  authority_name      VARCHAR(250) NOT NULL,
  CONSTRAINT unique_authority_name UNIQUE (authority_name)
);

CREATE TABLE user_authorities (
  user_id      BIGINT NOT NULL,
  authority_id BIGINT NOT NULL,
  CONSTRAINT fk_authorities_users FOREIGN KEY (user_id) REFERENCES users (id),
  CONSTRAINT fk_group_authorities_authority FOREIGN KEY (authority_id) REFERENCES authorities (id),
  CONSTRAINT unique_user_authorities UNIQUE (user_id, authority_id)
);

CREATE TABLE user_sessions (
  id                     BIGSERIAL PRIMARY KEY,
  user_id             BIGINT        NOT NULL,
  token               VARCHAR(2500) NOT NULL,
  expires             BIGINT        NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users
);