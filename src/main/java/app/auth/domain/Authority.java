package app.auth.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by almagnit on 11.09.2016.
 */
@Entity
@Table(name = "authorities")
@EqualsAndHashCode(callSuper = true)
public class Authority extends AppAbstractPersistable implements GrantedAuthority {


    public Authority() {
        super();
    }

    public Authority(AuthorityType authorityName) {
        this.authorityName = authorityName;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "authority_name")
    private AuthorityType authorityName;

    @Override
    @JsonIgnore
    public String getAuthority() {
        return authorityName.name();
    }

    @Override
    public String toString() {
        return authorityName.toString();
    }

    public AuthorityType getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(AuthorityType authorityName) {
        this.authorityName = authorityName;
    }


}
