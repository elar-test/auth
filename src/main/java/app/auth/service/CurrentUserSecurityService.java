package app.auth.service;

import app.auth.domain.User;
import app.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author Aleksey
 * @since 06.10.2016 12:17
 */

@Service
public class CurrentUserSecurityService {

    @Autowired
    private UserRepository userRepository;

    public User getUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }

        Object details = getAuthentication().getDetails();

        if (details != null && details instanceof User) {
            User user = (User) details;
            return userRepository.findOneByUsername(user.getUsername());
        }

        return null;
    }

    public Authentication getAuthentication(){
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
